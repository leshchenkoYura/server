import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] args) throws InterruptedException,IOException {
        CountDownLatch latch = new CountDownLatch(1);
        new ServerJob();
        latch.await();
    }
}
