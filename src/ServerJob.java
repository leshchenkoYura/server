
import io.reactivex.Flowable;

import io.reactivex.schedulers.Schedulers;
import java.io.*;
import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



class ServerJob {
    private ServerSocket serverSocket = new ServerSocket(Constant.SOCKET_PORT);
    private List<Client> pull = new CopyOnWriteArrayList<>();
    private SimpleDateFormat sdf;


    ServerJob() throws IOException {
        sdf = new SimpleDateFormat(Constant.TIME_PATTERN);
        Flowable.interval(50, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(v -> Flowable.just(serverSocket.accept()))
                .flatMap(socket -> Flowable.just(new Client(socket)))
                .flatMap(client -> {
                    String cl = client.getIn().readUTF();
                    Message message = new Message();
                    message.setCmd(parseMessage(cl, Constant.PATTERN_COMMAND));
                    message.setName(parseMessage(cl, Constant.PATTERN_MESSAGE));
                    message.setDate(sdf.format(new Date()));
                    client.setLogin(message.getName());
                    pull.add(0, client);
                    return Flowable.just(message);
                })
                .filter(m -> m.getCmd().equals(Constant.CMD_LOGIN))
                .flatMap(this::sendMessage)
                .subscribe(v -> System.out.println(setLog(stateClient(v.getCmd()), v, true)),Throwable::getMessage);

        Flowable.interval(50, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(v -> Flowable.just(pull))
                .filter(f -> f.size() > 0)
                .flatMap(Flowable::fromIterable)
                .filter(v -> v.getIn().available() > 0)
                .flatMap(v -> {
                    String cl = v.getIn().readUTF();
                    Message message = new Message();
                    message.setCmd(parseMessage(cl, Constant.PATTERN_COMMAND));
                    message.setName(v.getLogin());
                    message.setMessage(parseMessage(cl, Constant.PATTERN_MESSAGE));
                    message.setDate(sdf.format(new Date()));
                    message.setIndex(pull.indexOf(v));
                    return Flowable.just(message);
                })
                .flatMap(v -> {
                    if (v.getCmd().equals(Constant.CMD_EXIT)) {
                        pull.get(v.getIndex()).getOut().close();
                        pull.get(v.getIndex()).getIn().close();
                        pull.get(v.getIndex()).getCs().close();
                        pull.remove(v.getIndex());
                    }
                    return Flowable.just(v);
                })
                .flatMap(this::sendMessage)
                .subscribe(v -> System.out.println(setLog(stateClient(v.getCmd()), v, true)), System.out::println);
    }


    private synchronized Flowable<Message> sendMessage(Message message) {
        pull.forEach(v -> {
            try {
                v.getOut().writeUTF(message.getDate()
                        .concat(" ")
                        .concat(message.getName())
                        .concat(" ")
                        .concat(stateClient(message.getCmd()))
                        .concat(" ")
                        .concat(message.getCmd().equals(Constant.CMD_MESSAGE) ? message.getMessage() : ""));
                v.getOut().flush();
            } catch (IOException e) {
                pull.remove(v);
            }
        });
        return Flowable.just(message);
    }

    private String stateClient(String state) {
        switch (state) {
            case Constant.CMD_LOGIN:
                return Constant.LOG_LOGIN;
            case Constant.CMD_MESSAGE:
                return Constant.LOG_MESSAGE;
            case Constant.CMD_EXIT:
                return Constant.LOG_EXIT;
            default:
                return "";
        }
    }

    private String parseMessage(String message, String pattern) {
        if (message != null && !message.isEmpty()) {
            Pattern mPattern = Pattern.compile(pattern);
            Matcher matcher = mPattern.matcher(message);
            if (matcher.find()) {
                message = matcher.group();
            }
            return message.replaceAll(":", " ").trim();
        } else {
            return "";
        }
    }


    private synchronized Message setLog(String cmd, Message message, boolean flag) {
        PrintWriter printWriter = null;
        File file = new File(Constant.PATH);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(Constant.PATH, flag));
            printWriter.write(Constant.NEW_LINE
                    .concat(message.getDate())
                    .concat(" имя ")
                    .concat(message.getName())
                    .concat(" ")
                    .concat(cmd)
                    .concat(message.getMessage()));
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
        return message;
    }
}
