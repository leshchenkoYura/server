 abstract class Constant {
     final static int SOCKET_PORT = 7777;
     final static String PATTERN_COMMAND = ".+?:";
     final static String PATTERN_MESSAGE = ":.+";
     final static String CMD_LOGIN = "login";
     final static String CMD_MESSAGE = "msg";
     final static String CMD_EXIT = "exit";
     final static String TAG_CONNECT_CLIENT = "client connected ";
     final static String TAG_EXIT_CLIENT = "client exit ";
     final static String PATH = "logChat.txt";
     final static String LOG_MESSAGE = "Сообщение:";
     final static String LOG_LOGIN = "Пользователь подключился ";
     final static String LOG_EXIT = "Пользователь вышел ";
     final static String TIME_PATTERN = "EEE, d MMM yyyy HH:mm:ss";
     final static String NEW_LINE = System.getProperty("line.separator");
}
